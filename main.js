const input = document.querySelector(".input");
const submitButton = document.querySelector(".btn");
const todoList = document.querySelector(".todo-list")

// Event listeners
document.addEventListener("DOMContentLoaded", getTodos);


submitButton.addEventListener("click", addTodo);

todoList.addEventListener("click", listModification);

//Functions

function addTodo(e) {

    e.preventDefault();

    if (input.value === "") {
        return
    }
    const todoDiv = document.createElement("div");
    todoDiv.classList.add("todo-div")
    const listTodo = document.createElement("li");
    listTodo.innerText = input.value;

    saveLocalTodos(input.value);

    listTodo.classList.add("items");
    // listTodo.id = Math.random


    todoDiv.appendChild(listTodo);
    input.value = "";
    const completeBtn = document.createElement("button");
    completeBtn.innerHTML = `<i class="fas fa-check"></i>`;
    completeBtn.classList.add("complete-btn");

    const trashBtn = document.createElement("button");
    trashBtn.innerHTML = `<i class="fas fa-trash"></i>`;
    trashBtn.classList.add("trash-btn");

    const editBtn = document.createElement("button");
    editBtn.innerHTML = `<i class="fas fa-edit"></i>`;
    editBtn.classList.add("edit-btn");

    todoDiv.appendChild(completeBtn);
    todoDiv.appendChild(trashBtn);
    todoDiv.appendChild(editBtn);

    todoList.appendChild(todoDiv);

}

function listModification(e) {

    const targetElement = e.target;
    // console.log(targetElement)
    // const todo = targetElement.parentElement
    if (targetElement.classList[0] === "trash-btn") {
        const todo = targetElement.parentElement
        removeLocalTodos(todo);
        targetElement.parentElement.remove();

    } else if (targetElement.classList[0] === "complete-btn") {
        targetElement.parentElement.classList.toggle("completed");

        console.log(targetElement.parentElement);

    } else if (targetElement.classList[0] === "edit-btn") {
        // console.log(targetElement.parentElement.firstElementChild)
        const targetLi = targetElement.parentElement.firstElementChild;
        const editText = targetLi.textContent;
        // console.log(editText)

        const input = document.querySelector(".input");
        // console.log(input)
        input.value = editText;
        const todo = targetElement.parentElement
        removeLocalTodos(todo);
        targetElement.parentElement.remove();

        // console.log("in")

    }
}

function saveLocalTodos(todo) {
    let todos;
    if (localStorage.getItem("todos") === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem("todos"));
    }
    todos.push(todo);
    localStorage.setItem("todos", JSON.stringify(todos));
}

function removeLocalTodos(todo) {
    let todos;
    if (localStorage.getItem("todos") === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem("todos"));
    }
    const todoIndex = todo.children[0].innerText;
    todos.splice(todos.indexOf(todoIndex), 1);
    localStorage.setItem("todos", JSON.stringify(todos));
}

// function saveCompletedTodos(todo) {
//     let todos;
//     if (localStorage.getItem("todos") === null) {
//         todos = [];
//     } else {
//         todos = JSON.parse(localStorage.getItem("todos"));
//     }

// }

function getTodos() {
    let todos;
    if (localStorage.getItem("todos") === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem("todos"));
    }
    todos.forEach(function(todo) {
        //Create todo div
        const todoDiv = document.createElement("div");
        todoDiv.classList.add("todo-div")
        const listTodo = document.createElement("li");
        listTodo.innerText = todo;

        // saveLocalTodos(todoInput.value);

        listTodo.classList.add("items");
        // listTodo.id = Math.random

        todoDiv.appendChild(listTodo);
        input.value = "";
        const completeBtn = document.createElement("button");
        completeBtn.innerHTML = `<i class="fas fa-check"></i>`;
        completeBtn.classList.add("complete-btn");

        const trashBtn = document.createElement("button");
        trashBtn.innerHTML = `<i class="fas fa-trash"></i>`;
        trashBtn.classList.add("trash-btn");

        const editBtn = document.createElement("button");
        editBtn.innerHTML = `<i class="fas fa-edit"></i>`;
        editBtn.classList.add("edit-btn");

        todoDiv.appendChild(completeBtn);
        todoDiv.appendChild(trashBtn);
        todoDiv.appendChild(editBtn);
        todoList.appendChild(todoDiv);
    });
}